
<?php

//object use kore value access kora
class BITM{
    public $window;
    public $door;
    public $desk;
    public $whiteboard;


    public function cooling_process(){

        echo"i'm cooling the room.";
    }

    public function compute(){
        echo"i'm computing the equation";

    }

    public function show(){
        echo"i'm showing details of BITM: <br>";
        echo $this->desk."<br>";
        echo $this->door."<br>";
        echo $this->window."<br>";
        echo $this->whiteboard."<br>";
    }

    public function do_something(){
        echo"Do something";
    }

    public function setDoor($door_num)
    {
        $this->door = $door_num;
    }

    public function setWhiteboard($whiteboard_num)
    {
        $this->whiteboard = $whiteboard_num;
    }

    public function setDesk($desk_num)
    {
        $this->desk = $desk_num;
    }

    public function setWindow($window_num)
    {
        $this->window = $window_num;
    }


}//end of class


$obj_BITM_at_ctg= new BITM;

$obj_BITM_at_ctg->setDesk("total chair = 10");
$obj_BITM_at_ctg->setDoor("total door = 2");
$obj_BITM_at_ctg->setWhiteboard("total chair= 30");
$obj_BITM_at_ctg->setWindow("total window = 5");
$obj_BITM_at_ctg->show();

//another class

class BITM_lab_402 extends BITM{

    public function child_chair(){
       parent::do_something();
    }

}

$obj_lab_402_in_BITM= new BITM_lab_402();
//$obj_lab_402_in_BITM->compute();
$obj_lab_402_in_BITM->setDesk("chair = 10");
$obj_lab_402_in_BITM->setDoor("door = 2");
$obj_lab_402_in_BITM->setWhiteboard("chair= 30");
$obj_lab_402_in_BITM->setWindow("window = 5");
$obj_lab_402_in_BITM->show();
$obj_lab_402_in_BITM->child_chair();








