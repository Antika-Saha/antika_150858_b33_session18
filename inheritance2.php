<?php

class BITM{
    public $window;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;


    public function coolTheAir(){
        echo "I'm cooling the air";
    }

    public function compute(){
        echo "I'm computing";
    }

    public function show(){
        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->table."<br>";
        echo $this->chair."<br>";
        echo $this->whiteboard."<br>";
    }
//value korte set chair fn likha hoice.
    public function setChair($chair)
    {

        $this->chair = $chair. " [ set by parent]" ;
    }

    public function setDoor($door)
    {
        $this->door = $door;
    }

    public function setWindow($win)
    {
        $this->window = $win;
    }

    public function setTable($tab)
    {
        $this->table = $tab;
    }

    public function setWhiteboard($board)
    {
        $this->whiteboard = $board;
    }


}// end of class BITM


class BITM_Lab402 extends BITM {

    public function setChairFromChild($chair)
    {
        $chair = $chair . " [ set by child] ";

        parent::setChair($chair);

    }

}

$obj_BITM_at_Ctg =  new BITM;
$obj_BITM_at_Ctg->setChair("I'm a chair");
$obj_BITM_at_Ctg->setTable("I'm a Table");
$obj_BITM_at_Ctg->setWindow("I'm a Window");
$obj_BITM_at_Ctg->setDoor("I'm a Door");
$obj_BITM_at_Ctg->setWhiteboard("I'm a Whiteboard");
$obj_BITM_at_Ctg->show();




$objBITM_Lab = new BITM_Lab402();
$objBITM_Lab->setChair("This is a chair");
$objBITM_Lab->setTable("This is a Table");
$objBITM_Lab->setWindow("This is a Window");
$objBITM_Lab->setDoor("This is a Door");
$objBITM_Lab->setWhiteboard("This is a Whiteboard");
$objBITM_Lab->show();



echo $objBITM_Lab->chair . "<br>";

$objBITM_Lab->setChairFromChild("Ami Ekti Chair");


echo $objBITM_Lab->chair. "<br>";











