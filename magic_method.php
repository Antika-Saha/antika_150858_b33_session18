<?php

class ImAclass{
    public static $mymsg;
    public  static function message(){
        echo self::$mymsg;
    }

    public function __construct($value)
    {
        echo $value."<br>";
    }

    public function  __destruct()
    {
        echo"<br> good bye";
    }

    public function __call($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }

    public function __set($name, $value)
    {
        echo $name."<br>";
        print_r($value);
    }

}

$obj1=new ImAclass("hello world!!!");
$obj1->show(29,55,"oiiii");
ImAclass::$mymsg="data is deleted";
ImAclass::message();
